﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Threading;
using System.Diagnostics;
using System.IO;
namespace TaskDaemon
{
    class MonitorProcess
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        string _name = string.Empty;
        string _exe = string.Empty;
        string _windowname = string.Empty;
        string _start = string.Empty;
		string _folder = string.Empty;
        int _startpausems = 0;

        public MonitorProcess(XmlNode n)
        {
            foreach (XmlNode n1 in n.ChildNodes)
            {
                if (n1.NodeType != XmlNodeType.Comment)
                {
                    if (n1.Name.ToLower() == "name")
                    {
                        _name = n1.InnerText.ToLower();
                    }
                    else if (n1.Name.ToLower() == "executable")
                    {
                        _exe = n1.InnerText.ToLower();
                    }
                    else if (n1.Name.ToLower() == "windowname")
                    {
                        _windowname = n1.InnerText.ToLower();
                    }
                    else if (n1.Name.ToLower() == "folder")
                    {
                        _folder = n1.InnerText.ToLower();
                        if (!Directory.Exists(_folder))
                        {
                            log.Warn("Start folder '" + _folder + "' does not exist.\n");
                        }
                    }
                    else if (n1.Name.ToLower() == "start")
                    {
                        _start = n1.InnerText.ToLower();
                    }
                    else if (n1.Name.ToLower() == "startpausems")
                    {
                        try
                        {
                            _startpausems = int.Parse(n1.InnerText);
                        }
                        catch (Exception ex)
                        {
                            throw new ApplicationException("Error parsing Start Pause MS delay '" + n1.InnerText + "'\n" + ex.Message);
                        }
                    }
                    else
                    {
                        log.Warn("Unrecognised config element:" + n1.Name);
                    }
                }
            }

        }

        public bool Check()
        {
            Process[] processes = Process.GetProcesses();

            if (_exe != string.Empty)
            {
                bool found = false;

                foreach (Process p in processes)
                {
                    try
                    {
                        if (Path.GetFileName(p.MainModule.FileName.ToLower()) == _exe)
                        {
                            found = true;
                            break;
                        }
                    }
                    catch(Exception ex)
                    {
                        // ignore these
                    }

                    if (!found && p.ProcessName.ToLower() == _name)
                    {
                        found = true;
                        break;
                    }
                }

                if (!found)
                {
                    log.Error("Process " + _exe + " not found.");
                    return false;
                }
            }

            if (_windowname != string.Empty)
            {
                bool found = false;
                foreach (Process p in processes)
                {
                    if (p.MainWindowTitle.ToLower() == _windowname)
                    {
                        found = true;
                        break;
                    }
                }

                if (!found)
                {
                    log.Info("Window " + _windowname + " not found.");
                    return false;
                }
            }

            return true;
        }

        public void CheckAndStart()
        {
            if (!Check())
            {
                Start();
            }
        }

        public void Start()
        {
            ProcessStartInfo psi = new ProcessStartInfo(_start);
            if (_folder != "" && Directory.Exists(_folder))
            {
                log.Info("Starting " + _start + " in " + _folder);
                psi.WorkingDirectory = _folder;
            }
            else
            {
                log.Info("Starting " + _start);
            }

            try
            {
                Process.Start(psi);
            }
            catch(Exception ex)
            {
                log.Error("Exception starting process " + _start, ex);
            }
            Thread.Sleep(_startpausems);
        }
    }
}
