﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Threading;
using System.IO;

namespace TaskDaemon
{
    class Program
    {
        const string __configfile = "TaskDaemon.xml";
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        static int _checkintervalms = 5000;
        static List<MonitorProcess> _processes = new List<MonitorProcess>();

        static void Main(string[] args)
        {
            log4net.Config.XmlConfigurator.ConfigureAndWatch(new FileInfo(__configfile));
            log.Info("TaskDaemon started.");

            XmlDocument config = new XmlDocument();
            try
            {
                config.Load(__configfile);
            }
            catch (Exception ex)
            {
                log.Fatal("Error: " + __configfile + " could not be loaded.", ex);
                return;
            }

            try
            {
                foreach (XmlNode n in config.ChildNodes)
                {
                    if (n.NodeType != XmlNodeType.Comment)
                    {
                        if (n.Name.ToLower() == "taskdaemon")
                        {
                            foreach (XmlNode n1 in n.ChildNodes)
                            {
                                if (n1.NodeType != XmlNodeType.Comment)
                                {
                                    if (n1.Name.ToLower() == "checkintervalms")
                                    {
                                        try
                                        {
                                            _checkintervalms = int.Parse(n1.InnerText);
                                        }
                                        catch(Exception ex)
                                        {
                                            throw new ApplicationException("Error parsing CheckIntervalMS", ex);
                                        }
                                    }
                                    else if (n1.Name.ToLower() == "processes")
                                    {
                                        foreach (XmlNode n2 in n1.ChildNodes)
                                        {
                                            if (n2.NodeType != XmlNodeType.Comment)
                                            {
                                                if (n2.Name.ToLower() == "process")
                                                {
                                                    _processes.Add(new MonitorProcess(n2));
                                                }
                                                else
                                                {
                                                    log.Warn("Unrecognised config element:" + n2.Name);
                                                }

                                            }
                                        }
                                    }
                                    else if (n1.Name.ToLower() == "log4net")
                                    {
                                        // ignore this node
                                    }
                                    else
                                    {
                                        log.Warn("Unrecognised config element:" + n1.Name);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Fatal("Error loading configuration.", ex);
                return;
            }

            while(true)
            {
                foreach(MonitorProcess mp in _processes)
                {
                    mp.CheckAndStart();
                }

                Thread.Sleep(_checkintervalms);
            }

            log.Info("TaskDaemon exiting.");
        }
    }
}
